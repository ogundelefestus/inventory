<?php
//DB TIME TO HUMAN READABLE TIME HELPER
use Illuminate\Support\Carbon;


if (! function_exists('db_to_human_time')) {
    function db_to_human_time($time) {
        $dt=Carbon::createFromFormat('Y-m-d H:i:s', $time);
        return $dt->diffForHumans();
    }
}



