<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("login");
});
Auth::routes(['register' => false]);

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


Route::group([
    'middleware' => ['auth', 'role:laboratorymanager'],
    'namespace'=>'User',
    'prefix'=>'user',
], function() {

    //System Setting

    Route::get('/setting', 'SettingController@showSettings')->name('show.setting');
    Route::post('/create/setting', 'SettingController@postSettings')->name('post.setting');

//User Management Coordinator
    Route::get('user', 'UserController@showManager')->name('show.user');
    Route::get('list-user', 'UserController@showListManager')->name('show_list.user');
    Route::post('create/user', 'UserController@createManager')->name('create.user');
    Route::get('/delete_manager/{user?}', 'UserController@deleteManager')->name('manager.delete');
    Route::post('/edit_admin/{user?}', 'UserController@editAdmin')->name('admin.edit');


//User Management Student
    Route::get('student', 'UserController@showStudent')->name('show.student');
     Route::get('list-student', 'UserController@showListStudent')->name('show_list.student');
    Route::post('create/student', 'UserController@createStudent')->name('create.student');
    Route::get('/delete_student/{user}', 'UserController@deleteStudent')->name('student.delete');




//Product Management
    Route::get('product', 'ProductController@showProduct')->name('show.product');
    Route::get('list-product', 'ProductController@showListProduct')->name('show_list.product');
    Route::post('create/product', 'ProductController@createProduct')->name('create.product');
    Route::get('/product/{id}', 'ProductController@deleteProduct')->name('product.delete');


//Product Units
    Route::get('units', 'UnitController@showUnit')->name('show.unit');
    Route::post('create/unit', 'UnitController@createUnit')->name('create.unit');
    Route::get('/unit/{id}', 'UnitController@deleteUnit')->name('unit.delete');
    Route::post('/edit_unit/{unit?}', 'UnitController@editUnit')->name('unit.edit');

//Product Categories
    Route::get('categories', 'CategoryController@showCategory')->name('show.category');
    Route::post('create/category', 'CategoryController@createCategory')->name('create.category');
    Route::get('/category/{id}', 'CategoryController@deleteCategory')->name('category.delete');








});






Route::group([
    'middleware' => ['auth', 'role:student'],
    'namespace'=>'User',
    'prefix'=>'user',
], function() {


    //Request Management
    Route::get('/show_request', 'RequestController@showRequest')->name('show.request');
    Route::post('/create_request', 'RequestDevicesController@createRequest')->name('create.request');




});



Route::resource('export', 'ExportController');
