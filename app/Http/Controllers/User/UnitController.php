<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Requests\UnitRequest;
use App\Unit;
use Illuminate\Routing\Controller;

class UnitController extends Controller
{
    

    public function showUnit(){
        $units=Unit::all();
        return view('user.units',compact('units'));

    }

    public function createUnit(Request $request){
          $request->validate([
            "name"=>"required",
            "short_name"=>"required",
        ]);
        $unit=$request->all();
        $unit=Unit::create($unit);
        if($unit->save()){
            return back()->with('status','Unit Created Successfully');
        }
            return back()->with('danger','Unit Not Created ');
    }

    public function deleteUnit($id){
        $unit=Unit::where('id',$id)->firstorFail()->delete();
        return back()->with('status','Unit Deleted Successfully');
    }

    public function  editUnit(Request $request, Unit $unit){

        $request->validate([
            "name"=>"required",
            "short_name"=>"required",
        ]);
        $unit->update($request->all());
        return back()->with('status','Unit Updated Successfully');


    }

}
