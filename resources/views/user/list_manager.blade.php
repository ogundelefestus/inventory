@extends('layouts.dashboard')

@section('content')
<div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">List Of Managers</h4>
                
                  <div class="table-responsive pt-3">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>
                            #
                          </th>
                          <th>
                            First name
                          </th>
                          <th>
                            Progress
                          </th>
                          <th>
                            Amount
                          </th>
                          <th>
                            Delete
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                  @foreach($manager as $count=>$managers)
                        <tr>
                          <td>
                            {{$count+1}}
                          </td>
                          <td>
                            {{$managers->name}}
                          </td>
                          <td>
                            {{$managers->email}}
                          </td>
                          <td>
                            {{db_to_human_time($managers->created_at)}}
                          <td>
                            <a href="{{route("manager.delete",$managers->id)}}">Delete</a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
        </div>

        @endsection