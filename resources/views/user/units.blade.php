@extends('layouts.dashboard')
@section('content')
 <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">

                <div class="card-body">
                  @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('danger'))
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    @endif
                  <h4 class="card-title">Create Units </h4>
                  <p class="card-description">
                    To Manage the Laboratory
                  </p>
                  <form class="forms-sample" action="{{route('create.unit')}}" method="POST">
                    @csrf
                    <div class="form-group">
                      <label for="exampleInputName1">Units</label>
                      <input type="text" class="form-control" id="exampleInputName1" placeholder="Name(Pieces)"  name="name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail3">Short name</label>
                      <input type="text" class="form-control" id="exampleInputEmail3" placeholder="Short Name (pcs)"   name="short_name" >
                    </div>

                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>




            </div>
          </div>


      @endsection
