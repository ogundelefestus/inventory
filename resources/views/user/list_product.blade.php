@extends('layouts.dashboard')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">List Of Disused Electronic Devices <a href="/export"> Export Information</a></h4>

                            <div class="table-responsive pt-3">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Disused Electronic Device Name
                                        </th>
                                        <th>
                                            Disused Electronic Device Price
                                        </th>

                                        <th>
                                            Disused Electronic Device Image
                                        </th>

                                        <th>
                                            Disused Electronic Device Unit
                                        </th>

                                        <th>
                                            Disused Electronic Device Category
                                        </th>

                                        <th>
                                            Disused Electronic Device Description
                                        </th>
                                        <th>
                                           Created At
                                        </th>
                                        <th>
                                            Delete
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($product as $count=>$products)
                                        <tr>
                                            <td>
                                                {{$count+1}}
                                            </td>
                                            <td>
                                                {{$products->name}}
                                            </td>
                                            <td>
                                                &#8358;{{$products->price}}
                                            </td>

                                            <td>
                                                <img src="{{Storage::url($products->image)}}">
                                            </td>
                                            <td>
                                                &nbsp;{{$products->unit->name}}</p>
                                            </td>
                                            <td>
                                                &nbsp;{{$products->category->name}}</p>
                                            </td>

                                            <td>
                                                &nbsp;{{$products->description}}</p>
                                            </td>

                                            <td>
                                            {{db_to_human_time($products->created_at)}}
                                            <td>
                                                <a href="{{route("product.delete",$products->id)}}">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

@endsection
