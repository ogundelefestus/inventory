<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable=['name','price','image','unit_id','category_id'];


    public function  category(){

        return $this->belongsTo(Category::class);
    }

    public function  unit(){

        return $this->belongsTo(Unit::class);
    }
}
