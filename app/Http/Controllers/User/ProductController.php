<?php

namespace App\Http\Controllers\User;

use App\Department;
use App\Employee;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Notifications\NewProduct;
use App\Product;
use App\Category;
use App\User;
use App\Unit;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ProductController extends Controller
{
    /**
     * Author:Venus Software Solutions
     * Date:15/01/2021
     */

    /**Product**/
    public function showProduct()
    {
        $units = Unit::all();
        $products = Product::all();
        $categories= Category::all();
        return view('user.product', compact('products', 'units','categories'));
    }

    public function createProduct(Request $request)
    {

        $request->validate([
            "name"=>"required",
            "price"=>"required|numeric",
            "image"=>"required",
            "description"=>"required",
        ]);




        $path = $request->file('image')->store('public/product');

        //dd($path);


        $data = new Product;
        $data->name = $request->post('name');
        $data->price = $request->post('price');
        $data->unit_id= $request->post('unit_id');
        $data->category_id= $request->post('category_id');
        $data->description= $request->post('description');
        $data->image = $path;

        if ($data->save()) {

            return back()->with('status', 'Product Created Successfully');

        }

        return back()->with('danger', 'Product Not Created');
    }

    public function deleteProduct($id)
    {
        $product = Product::where('id', $id)->firstorfail()->delete();
        return back()->with('status', 'Product Deleted Successfully');
    }



    public function showListProduct(){

        $product=Product::all();
        return view('user.list_product',compact('product'));
    }

}
