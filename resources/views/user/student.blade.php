@extends('layouts.dashboard')
@section('content')
 <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">

                <div class="card-body">
                  @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('danger'))
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    @endif
                  <h4 class="card-title">Create Student</h4>
                  <p class="card-description">

                  </p>
                  <form class="forms-sample" action="{{route('create.user')}}" method="POST" >
                    @csrf
                    <div class="form-group">
                      <label for="exampleInputName1">Student Name</label>
                      <input type="text" class="form-control" id="exampleInputName1" placeholder="Name" name="name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail3">Student Email address</label>
                      <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email" name="email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword4">Student Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword4" placeholder="Password" name="password">
                    </div>


                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>




            </div>
          </div>


      @endsection
