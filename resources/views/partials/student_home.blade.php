<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="row">
                    <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                        <h3 class="font-weight-bold">Welcome {{\Illuminate\Support\Facades\Auth::user()->name }} Student</h3>
                        <h6 class="font-weight-normal mb-0">Inventory management system for disused elecrtronics devices in a computer lab </h6>
                    </div>
                    <div class="col-12 col-xl-4">
                        <div class="justify-content-end d-flex">
                            <div class="dropdown flex-md-grow-1 flex-xl-grow-0">
                                <button class="btn btn-sm btn-light bg-white dropdown-toggle" type="button" id="dropdownMenuDate2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="mdi mdi-calendar"></i> Today (10 Jan 2021)
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDate2">
                                    <a class="dropdown-item" href="#">January - March</a>
                                    <a class="dropdown-item" href="#">March - June</a>
                                    <a class="dropdown-item" href="#">June - August</a>
                                    <a class="dropdown-item" href="#">August - November</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card tale-bg">
                    <div class="card-people mt-auto">
                        <img src="images/dashboard/people.svg" alt="people">
                        <div class="weather-info">
                            <div class="d-flex">
                                {{--                      <div>--}}
                                {{--                        <h2 class="mb-0 font-weight-normal"><i class="icon-sun mr-2"></i>31<sup>C</sup></h2>--}}
                                {{--                      </div>--}}
                                <div class="ml-2">
                                    <h4 class="location font-weight-normal">Osogbo</h4>
                                    <h6 class="font-weight-normal">Nigeria</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 grid-margin transparent">
                <div class="row">
                    <div class="col-md-6 mb-4 stretch-card transparent">
                        <div class="card card-tale">
                            <div class="card-body">
                                <p class="mb-4">Total Student</p>
                                <p class="fs-30 mb-2">{{$userCount}}</p>
                                {{--                      <p>10.00% (30 days)</p>--}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-4 stretch-card transparent">
                        <div class="card card-dark-blue">
                            <div class="card-body">
                                <p class="mb-4">Total Number of Disused Electronic Devices</p>
                                <p class="fs-30 mb-2">{{$laboratorymanagerCount}}</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">List Of Disused Electronic Devices In The Department of Information And Communication Technology.<a href="/export"> Export Information</a></h4>

                        <div class="table-responsive pt-3">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Disused Electronic Device Name
                                    </th>
                                    <th>
                                        Disused Electronic Device Price
                                    </th>

                                    <th>
                                        Disused Electronic Device Image
                                    </th>

                                    <th>
                                        Disused Electronic Device Unit
                                    </th>

                                    <th>
                                        Disused Electronic Device Category
                                    </th>
                                    <th>
                                        Created At
                                    </th>

                                    <th>
                                        Request For Disused Electronic Devices
                                    </th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($product as $count=>$products)
                                    <tr>
                                        <td>
                                            {{$count+1}}
                                        </td>
                                        <td>
                                            {{$products->name}}
                                        </td>
                                        <td>
                                            &#8358;{{$products->price}}
                                        </td>

                                        <td>
                                            <img src="{{Storage::url($products->image)}}" >
                                        </td>
                                        <td>
                                            &nbsp;{{$products->unit->name}}</p>
                                        </td>
                                        <td>
                                            &nbsp;{{$products->category->name}}</p>
                                        </td>

                                        <td>
                                        {{db_to_human_time($products->created_at)}}
                                        </td>

                                        <td>
                                           Request
                                        </td>
                                       <td></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
