<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $laboratorymanagerCount=User::role("laboratorymanager")->count();
        $studentCount=User::role("student")->count();
        $userCount=User::count();

        $productCount=Product::count();
        $user=User::all();

        if(User::role("student")){

            $product=Product::all();

        }


        return view('home',compact('productCount','userCount','laboratorymanagerCount','user','studentCount','product'));
    }
}
