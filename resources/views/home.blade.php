@extends('layouts.dashboard')

@section('content')
    @role('laboratorymanager')
    @include("partials.manager_home")
    @endrole

    @role('student')
    @include("partials.student_home")
    @endrole
@endsection
