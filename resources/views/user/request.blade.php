@extends('layouts.dashboard')
@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            @if (session('danger'))
                                <div class="alert alert-danger">
                                    {{ session('danger') }}
                                </div>
                            @endif
                            <h4 class="card-title">Borrow Request From The Admin</h4>

                            <form class="forms-sample" action="{{route('create.request')}}" method="POST" >
                                @csrf

                                <div class="form-group">
                                    <label for="exampleSelectGender">Select Disused Electronic Devices </label>
                                    <select class="form-control" id="exampleSelectGender" name="product_id">
                                        @foreach($product as $products)
                                            <option
                                                value="{{$products->id}}">{{$products->name}}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="exampleTextarea1">Reason For Borrowing</label>
                                    <textarea class="form-control" id="exampleTextarea1" name="reason" rows="4"></textarea>
                                </div>
                                {{--                                <div class="form-group">--}}
                                {{--                                    <label>File upload</label>--}}
                                {{--                                    <input type="file" name="image" class="file-upload-default">--}}
                                {{--                                    <div class="input-group col-xs-12">--}}
                                {{--                                        <input type="text" name="image" class="form-control file-upload-info" disabled placeholder="Upload Image">--}}
                                {{--                                        <span class="input-group-append">--}}
                                {{--                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>--}}
                                {{--                        </span>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}

                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>




            </div>
        </div>


@endsection
