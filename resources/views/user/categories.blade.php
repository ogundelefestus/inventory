@extends('layouts.dashboard')
@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            @if (session('danger'))
                                <div class="alert alert-danger">
                                    {{ session('danger') }}
                                </div>
                            @endif
                            <h4 class="card-title">Create Category </h4>
                            <p class="card-description">
                                To Manage the Laboratory
                            </p>
                            <form class="forms-sample" action="{{route('create.category')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputName1">Category Name</label>
                                    <input type="text" class="form-control" id="exampleInputName1" placeholder="Category Name"  name="name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleTextarea1">Description</label>
                                    <textarea class="form-control" id="exampleTextarea1" rows="4" name="description" placeholder="Category Description"></textarea>
                                </div>

                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>




            </div>
        </div>


@endsection
