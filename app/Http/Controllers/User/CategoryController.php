<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{


    public function showCategory()
    {
        return view('user.categories');
    }


    public function createCategory(Request $request)
    {

        $request->validate([
            'name'=>'required',
            'description'=>'required'
        ]);
        $category=Category::create([
            'name'=>$request->name,
            'description'=>$request->description,
        ]);

        return back()->with('status','Category Created Successfully');
    }




    public function edit(Category $category)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }


    public function deleteCategory($id)
    {
        $category = Category::where('id', $id)->firstorfail()->delete();
        return back()->with('status','Category Deleted Successfully');
    }
}
