<?php

namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;


use App\RequestDevices;
use Illuminate\Http\Request;

class RequestDevicesController extends Controller
{
    public function createRequest(Request $request)
    {

        $request->validate([
            'reason' => 'required',

        ]);

        $data = new RequestDevices();
        $data->reason = $request->post('reason');
        $data->product_id = $request->post('product_id');
        $data->user_id = auth()->id();


        if ($data->save()) {

            return back()->with('status', 'Request Created Successfully');
        }


    }
}
