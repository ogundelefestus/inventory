<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestDevices extends Model
{
    protected $fillable=['reason','product_id','user_id'];

}
