<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Permission::create(['name' => 'create-user']);
        Permission::create(['name' => 'edit-user']);
        Permission::create(['name' => 'delete-user']);
        Permission::create(['name' => 'view-employee']);

        $permission = Permission::all();
        $role1 = Role::create(['name' => 'laboratorymanager']);
        $role1->givePermissionTo($permission);
        $user = User::create([
            'name' => 'Oganla Olarewaju',
            'email' => 'laboratorymanager@gmail.com',
            'password' => bcrypt('123456')
        ]);
        $user->assignRole($role1);

        $role2 = Role::create(['name' => 'student']);
        $role2->givePermissionTo($permission);
        $user = User::create([
            'name' => 'Oladipo Umar',
            'email' => 'student@gmail.com',
            'password' => bcrypt('123456')
        ]);
        $user->assignRole($role2);

    

    }
}
