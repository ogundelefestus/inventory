@extends('layouts.dashboard')
@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            @if (session('danger'))
                                <div class="alert alert-danger">
                                    {{ session('danger') }}
                                </div>
                            @endif
                            <h4 class="card-title">Create Product </h4>
                            <p class="card-description">
                                To Manage the Laboratory
                            </p>
                            <form class="forms-sample" action="{{route('create.product')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputName1">Disused Device Name</label>
                                    <input type="text" class="form-control" id="exampleInputName1" placeholder="Disused Device Name"  name="name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail3">Disused Device Price(₦)</label>
                                    <input type="number" class="form-control" id="exampleInputEmail3" placeholder="Disused Device Price(₦) "   name="price" >
                                </div>

                                <div class="form-group">
                                    <label for="exampleSelectGender">Select Unit</label>
                                    <select class="form-control"  name="unit_id"  id="exampleSelectGender">
                                        @foreach($units as $unit)
                                            <option value="{{$unit->id}}">{{$unit->name}}
                                                ({{$unit->short_name}})
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleSelectGender">Select Category </label>
                                    <select class="form-control" id="exampleSelectGender" name="category_id">
                                        @foreach($categories as $category)
                                            <option
                                                value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="exampleFormControlFile1">Upload Disused Devices Image</label>
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image">
                                </div>
                                <div class="form-group">
                                    <label for="exampleTextarea1">Description</label>
                                    <textarea class="form-control" id="exampleTextarea1" name="description" rows="4"></textarea>
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label>File upload</label>--}}
{{--                                    <input type="file" name="image" class="file-upload-default">--}}
{{--                                    <div class="input-group col-xs-12">--}}
{{--                                        <input type="text" name="image" class="form-control file-upload-info" disabled placeholder="Upload Image">--}}
{{--                                        <span class="input-group-append">--}}
{{--                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>--}}
{{--                        </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>




            </div>
        </div>


@endsection
