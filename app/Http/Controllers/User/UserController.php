<?php

namespace App\Http\Controllers\User;

use App\Department;
use App\Designation;
use App\Employee;
use App\Http\Controllers\Controller;
use App\User;
use App\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{




    /** Student **/

    public function showListStudent(){
        $student=User::role('student')->get();
        return view('user.list_student',compact('student'));
    }
    public function showStudent(){

        return view('user.student');
    }

    public function createStudent(Request $request, Student $student){
        $request->validate([
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'matric_number'=>'required',
            'password' => 'required',
        ]);
        $pass=$request->post('password');
        $conf_pass=$request->post('password_confirmation');
        if($pass!=$conf_pass){

            return back()->with('danger','Password and Confirm Password fields do not match');
        }else{



            DB::transaction(function () use($request) {

                $name=$request->post('name');
                $email=$request->post('email');
                $matric_number=$request->post('matric_number');
                $password=$request->post('password');
                $phone=$request->post('phone');

                $user=User::create([

                    'name' => $name,
                    'email' => $email,
                    'matric_number'=>$matric_number,
                    'password' => Hash::make($password)

                ]);
                $user->assignRole([
                    'name'=>'student'
                ]);
                $student=new Student;
                $student->phone=$phone;
                $student->user_id=$user->id;

                if($student->save()){

                    return back()->with('status','Student Created Successfully');

                }else{

                    return back()->with('danger','Student not Created');
                }
            });

        }
    }

    public function deleteStudent(User $user){

        if($user->delete()){
            return back()->with('status','Student Deleted Successfully');
        }
        return back()->with('danger','Student Not Deleted');

    }



    /** Coordinator **/
    public function showListManager(){
        $manager=User::role('laboratorymanager')->get();

        return view('user.list_manager',compact('manager'));
    }

    public function showManager(){


        return view('user.manager');
    }

    public function createManager(Request $request){

        $request->validate([
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required',
        ]);
        $data=$request->all();
        $data['password']= Hash::make($data['password']);
        $user= User::create($data);
        $user->assignRole(['laboratorymanager']);
        if($user->save()){
            return back()->with('status','Manager Created Successfully');
        }
        return back()->with('danger',' Manager Not Created');

    }


    public function deleteManager(User $user){

        if($user->delete()){
            return back()->with('status','Manager Successfully');
        }
        return back()->with('danger','Project Coordinator Not Deleted');

    }

    public function editAdmin(Request $request,User $user){
        $request->validate([
            "name"=>"required",
            "email"=>"required",
            "salary"=>"nullable"
        ]);
        //Checking if the email inputed is already in the db
        $email=User::where('email','=',$request->post("email"))->first();

        if($email!=NULL && $email->id!=$user->id){

            return back()->with('danger','Email Already Exist');
        }

        if(!empty($request->post("password"))){
            $user->email=$request->post("email");
            $user->name=$request->post("name");
            $user->password=bcrypt($request->post("password"));

            if($user->save()){
                return back()->with('status','User Updated Successfully');
            }

            return back()->with('danger','User Not Updated');
        }

        if(!empty($request->post("salary"))){
            $user->email=$request->post("email");
            $user->name=$request->post("name");
            $user->salary=$request->post("salary");

            if($user->save()){
                return back()->with('status','User Updated Successfully');
            }

            return back()->with('danger','User Not Updated');
        }

        $user->email=$request->post("email");
        $user->name=$request->post("name");
        if($user->save()){

            return back()->with('status','User Updated Successfully');
        }
        return back()->with('danger','User Not Updated ');



    }



}


